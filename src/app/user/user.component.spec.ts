import {ComponentFixture, fakeAsync, TestBed, tick, waitForAsync} from '@angular/core/testing';

import { UserComponent } from './user.component';
import {UserService} from "./user.service";
import {DataService} from "../shared/data.service";

describe('UserComponent', () => {
  let component: UserComponent;
  let fixture: ComponentFixture<UserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should use the user name from the service', () => {
    const userService = fixture.debugElement.injector.get(UserService);
    // @ts-ignore
    expect(userService.user.name).toEqual(component.user.name)
  });

  it('should display the user name if user is logged in', () => {
    component.isLoggedIn = true;
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('p').textContent).toContain(component.user.name);
  });

  it('should not display the user name if user is not logged in', () => {
    component.isLoggedIn = false;
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('p').textContent).not.toContain(component.user.name);
  });

  it('should not fetch data successfully if not called asynchronously', () => {
    const dataService = fixture.debugElement.injector.get(DataService);

    // when this method gets executed, we instead return the returnValue()
    const spy = spyOn(dataService, 'getDetails').and.returnValue(Promise.resolve('Data'));
    fixture.detectChanges();

    expect(component.data).toBe(undefined);
  });

  it('should fetch data successfully if called asynchronously', waitForAsync(() => {
    const dataService = fixture.debugElement.injector.get(DataService);

    // when this method gets executed, we instead return the returnValue()
    const spy = spyOn(dataService, 'getDetails').and.returnValue(Promise.resolve('Data'));
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.data).toBe('Data');
    }) // allows to react when all async tasks are finished
  }));

  it('should fetch data successfully if called asynchronously 2', fakeAsync(() => {
    const dataService = fixture.debugElement.injector.get(DataService);

    // when this method gets executed, we instead return the returnValue()
    const spy = spyOn(dataService, 'getDetails').and.returnValue(Promise.resolve('Data'));
    fixture.detectChanges();
    tick(); // finish all async tasks
    expect(component.data).toBe('Data'); // TODO should work
  }));
});
